package me.tereshko.luna_moth_tags;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LunaMothTagsApplication {
	public static void main(String[] args) {
		SpringApplication.run(LunaMothTagsApplication.class, args);
	}

}
